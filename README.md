docker-nethack-server
======================

Dockerfile for Public NetHack server

## Build

    docker build -t nh .

## Howto

    docker run --detach --name=nh --publish=23:23 nh
