FROM ubuntu:18.04

RUN \
  apt-get update && \
  apt-get install -y autoconf bison \
      bsdmainutils curl flex gcc gdb git groff libncursesw5-dev libsqlite3-dev make \
      ncurses-dev sqlite3 tar telnetd-ssl xinetd locales && \
  apt-get clean

RUN locale-gen en_US.UTF-8

ADD ./setup /setup

RUN git clone "https://github.com/paxed/dgamelaunch.git" && \
  cd /dgamelaunch && \
    git checkout 55bd7dc && \
      ./autogen.sh \
        --enable-sqlite \
        --enable-shmem \
        --with-config-file=/nethack/etc/dgamelaunch.conf && \
  make && \
    cp /setup/dgl-create-chroot . && \
      ./dgl-create-chroot && \
    cd .. && \
    rm -rf /dgamelaunch

RUN git clone "https://github.com/NetHack/NetHack.git" /tmp/nh && \
  cd /tmp/nh/ && \
    git checkout NetHack-3.7.0_WIP && \
    cd sys/unix && \
    cp /setup/hints/linux hints/linux && \
      sh setup.sh hints/linux  && \
    cd ../../ && \
    make fetch-lua && \
      test -d "lib/lua-5.3.5/src" || exit 0 && \
    make install && \
      cd / && rm -rf /tmp/nh

RUN ( \
    echo "service telnet" && \
    echo "{" && \
    echo "  socket_type = stream" && \
    echo "  protocol    = tcp" && \
    echo "  user        = root" && \
    echo "  wait        = no" && \
    echo "  server      = /usr/sbin/in.telnetd" && \
    echo "  server_args = -L /nethack/dgamelaunch" && \
    echo "  rlimit_cpu  = 120" && \
    echo "}" \
    ) > /etc/xinetd.d/dgl

VOLUME ["/nethack/nh/var", "/nethack/dgldir"]

EXPOSE 23

CMD ["xinetd", "-dontfork"]
